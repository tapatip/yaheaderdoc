
##yaHeaderDoc

yaHeaderDoc is a fork of the [open source HeaderDoc](http://www.opensource.apple.com/source/headerdoc/) text processing utility maintained by Apple. The *current* goal of yaHeaderDoc is to provide a version of the utility that includes more-recent patches including those provided by David Gatwood and mentioned in [Re: Where can I find the VCS version?](http://prod.lists.apple.com/archives/headerdoc-dev/2014/Mar/msg00009.html) on the headerdoc-dev mailing list:

[http://www.darwin-development.org/headerdoc_patches/Xcode_5/](http://www.darwin-development.org/headerdoc_patches/Xcode_5/)

##Purpose
yaHeaderDoc is an inline documentation parsing utility. The utility will read source code files and parse specially flagged documentation comment blocks that have been coded using yaHeaderDoc compliant tags.

##Installation
The original HeaderDoc utility can be installed (on Mac OSX) as part of the OSX Developer Tools installation. Once installed successfully, The following two utilities will be available:

* /usr/bin/headerdochtml
* /usr/bin/gatherheaderdoc

To install this updated version, or to install on a non-Mac OSX computer, you will need to build and install a fresh copy.

###Obtaining Source Code
Apple maintains archives of released versions of the open source HeaderDoc at the following url:

[http://www.opensource.apple.com/tarballs/headerdoc/](http://www.opensource.apple.com/tarballs/headerdoc/)

It's important to note that the source code available there isn't necessarily as current as the version that is supplied with the OSX Developer Tools. For instance, as of this writing, it appears that OSX (10.10) includes release 8.9.24 whereas the most-current unpatched source from Apple is 8.9.17. You can "sort-of"check which version is installed by executing the following command:

```sh
>headerdoc2html -v
HTML output mode.
---------------------------------------------------------------------
        HeaderDoc Version: 8.9

        headerDoc2HTML - $Revision: 1392429329 $
        Modules:
                APIOwner - $Revision: 1393368373 $
                AvailHelper - $Revision: 1392429329 $
                BlockParse - $Revision: 1394058014 $
                CPPClass - $Revision: 1298084577 $
                Constant - $Revision: 1298084577 $
                Dependency - $Revision: 1298084578 $
                Enum - $Revision: 1298084578 $
                Function - $Revision: 1299283925 $
                Header - $Revision: 1306450222 $
                HeaderElement - $Revision: 1394132755 $
                IncludeHash - $Revision: 1298084578 $
                LineRange - $Revision: 1392429329 $
                Method - $Revision: 1298084578 $
                MinorAPIElement - $Revision: 1306444400 $
                ObjCCategory - $Revision: 1298084578 $
                ObjCClass - $Revision: 1298084578 $
                ObjCContainer - $Revision: 1298084578 $
                ObjCProtocol - $Revision: 1298084578 $
                PDefine - $Revision: 1299283925 $
                ParseTree - $Revision: 1394133609 $
                ParserState - $Revision: 1394058015 $
                Struct - $Revision: 1298084579 $
                Typedef - $Revision: 1298084579 $
                Utilities - $Revision: 1393441083 $
                Var - $Revision: 1298084579 $
---------------------------------------------------------------------
```

The "HeaderDoc Version" string doesn't include the patch number! And all of those *$Revision* strings refer to `git` time stamps. Unfortunately, this isn't very useful and one of the goals of yaHeaderDoc is to correct this nonsense.

###Building
**WARNING: Executing the following install command will result in the replacement of your current installation. yaHeaderDoc does not currently install differently named binaries! **

With source code unpacked, enter the source directory and execute the following commands:

```sh
>make
>sudo make real install
```

##Documentation
Additional documentation can be found in the Documentation directory and in the installed man pages for `headerdoc2html` and `gatherheaderdoc`. Also, refer to the official online documentation source at the following url:

[HeaderDoc User Guide](https://developer.apple.com/library/mac/documentation/DeveloperTools/Conceptual/HeaderDoc/intro/intro.html)

## License
yaHeaderDoc is licensed under the Apple Public Source License (Version 2.0 - August 6, 2003).
